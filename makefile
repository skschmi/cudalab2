CC=/usr/local/cuda/bin/nvcc
INCLUDE=-I/usr/local/cuda/include \
        -I/usr/local/cuda/samples/common/inc

LIBDIR=-L/usr/local/cuda/lib64
LIBS=-lcudart

EXECUTABLE=sobel

$(EXECUTABLE): sobel-sschmidt.o sobel-sschmidt-cuda.o
	$(CC) $(INCLUDE) $(LIBDIR) sobel-sschmidt.o sobel-sschmidt-cuda.o -o $(EXECUTABLE) $(LIBS)
#	time -p ./$(EXECUTABLE)
#	diff result.ppm result_correct.ppm

sobel-sschmidt.o: sobel-sschmidt.c sobel-sschmidt-cuda.o
	$(CC) $(INCLUDE) $(LIBDIR) $(LIBS) -o sobel-sschmidt.o -c sobel-sschmidt.c

sobel-sschmidt-cuda.o: sobel-sschmidt-cuda.cu sobel-sschmidt-cuda.h
	$(CC) $(INCLUDE) $(LIBDIR) $(LIBS) -o sobel-sschmidt-cuda.o -c sobel-sschmidt-cuda.cu

clean:
	rm -f ./$(EXECUTABLE)
	rm -f ./result.ppm
	rm -f ./*.o
	
time:
	time -p ./$(EXECUTABLE)

diff:
	diff result.ppm result_correct.ppm