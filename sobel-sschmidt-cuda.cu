#include <stdio.h>
#include <time.h>
#include <sys/time.h>


/*
Description:
    Basic sobel algorithm on the CPU
*/
void sobel_algorithm_cpu_1(unsigned int* pic, unsigned int* result, int xsize, int ysize, int thresh) {

	int i, j, magnitude, sum1, sum2;

	for (i = 1;  i < ysize - 1; i++) {
		for (j = 1; j < xsize -1; j++) {
			
			int offset = i*xsize + j;
			
			sum1 =  pic[ xsize * (i-1) + j+1 ] -     pic[ xsize*(i-1) + j-1 ]
			  + 2 * pic[ xsize * (i)   + j+1 ] - 2 * pic[ xsize*(i)   + j-1 ]
			  +     pic[ xsize * (i+1) + j+1 ] -     pic[ xsize*(i+1) + j-1 ];
			
			sum2 = pic[ xsize * (i-1) + j-1 ] + 2 * pic[ xsize * (i-1) + j ] + pic[ xsize * (i-1) + j+1 ]
				  - pic[xsize * (i+1) + j-1 ] - 2 * pic[ xsize * (i+1) + j ] - pic[ xsize * (i+1) + j+1 ];
			
			magnitude =  sum1*sum1 + sum2*sum2;
			
			result[offset] = 255*(magnitude > thresh);
			
			//Color in the left, right, top, botom, sides black
			if(i == 1)       result[(i-1)*xsize + j] = 0;
			if(i == ysize-2) result[(i+1)*xsize + j] = 0;      
			if(j == 1)       result[i*xsize + (j-1)] = 0;
			if(j == xsize-2) result[i*xsize + (j+1)] = 0;

			//Color in the four corners.
			if(i == 1 && j == 1)             result[(i-1)*xsize + (j-1)] = 0;
			if(i == 1 && j == xsize-2)       result[(i-1)*xsize + (j+1)] = 0;
			if(i == ysize-2 && j == 1)       result[(i+1)*xsize + (j-1)] = 0;
			if(i == ysize-2 && j == xsize-2) result[(i+1)*xsize + (j+1)] = 0;
		}
		//fprintf(stderr, "sobel done: %d/%d\n",i,(ysize-1-1));
	}
}



/*
Description:
    Basic sobel algorithm on the CPU, with slightly modified ordering of memory access
    to make the memory access contiguous without going back.
*/
void sobel_algorithm_cpu_2(unsigned int* pic, unsigned int* result, int xsize, int ysize, int thresh) {

	int i, j, magnitude, sum1, sum2;

	for (i = 1;  i < ysize - 1; i++) {
		for (j = 1; j < xsize -1; j++) {
			
			int offset = i*xsize + j;
			
			sum1 = 0;
			sum2 = 0;
    
			sum1 +=   -pic[xsize*(i-1) + j-1];
			sum2 +=    pic[xsize*(i-1) + j-1];   
			sum2 +=  2*pic[xsize*(i-1) + j  ];  
			sum1 +=    pic[xsize*(i-1) + j+1];
			sum2 +=    pic[xsize*(i-1) + j+1];

			sum1 += -2*pic[xsize*(i)   + j-1];
			sum1 +=  2*pic[xsize*(i)   + j+1];
    
			sum1 +=   -pic[xsize*(i+1) + j-1];
			sum2 +=   -pic[xsize*(i+1) + j-1];
			sum2 += -2*pic[xsize*(i+1) + j  ];
			sum1 +=    pic[xsize*(i+1) + j+1];
			sum2 +=   -pic[xsize*(i+1) + j+1];

			magnitude =  sum1*sum1 + sum2*sum2;
			
			result[offset] = 255*(magnitude > thresh);
			
			//Color in the left, right, top, botom, sides black
			if(i == 1)       result[(i-1)*xsize + j] = 0;
			if(i == ysize-2) result[(i+1)*xsize + j] = 0;      
			if(j == 1)       result[i*xsize + (j-1)] = 0;
			if(j == xsize-2) result[i*xsize + (j+1)] = 0;

			//Color in the four corners.
			if(i == 1 && j == 1)             result[(i-1)*xsize + (j-1)] = 0;
			if(i == 1 && j == xsize-2)       result[(i-1)*xsize + (j+1)] = 0;
			if(i == ysize-2 && j == 1)       result[(i+1)*xsize + (j-1)] = 0;
			if(i == ysize-2 && j == xsize-2) result[(i+1)*xsize + (j+1)] = 0;
		}
		//fprintf(stderr, "sobel done: %d/%d\n",i,(ysize-1-1));
	}
}



/*
Description:
    Basic global memory algorithm.  Each thread does one pixel of the image.
    */
__global__ void sobel_parallel_cuda_1(unsigned int* pic, unsigned int* result, int xsize, int ysize, int thresh) {

    int i, j, sum1, sum2, magnitude;    
    int offset = blockIdx.x*blockDim.x+threadIdx.x;
    i = offset / (xsize-2) + 1;
    j = offset % (xsize-2) + 1;
    int picindex = i*xsize + j;

    sum1 =  pic[ xsize * (i-1) + j+1 ] -     pic[ xsize*(i-1) + j-1 ]
      + 2 * pic[ xsize * (i)   + j+1 ] - 2 * pic[ xsize*(i)   + j-1 ]
      +     pic[ xsize * (i+1) + j+1 ] -     pic[ xsize*(i+1) + j-1 ];
    
    sum2 = pic[ xsize * (i-1) + j-1 ] + 2 * pic[ xsize * (i-1) + j ] + pic[ xsize * (i-1) + j+1 ]
          - pic[xsize * (i+1) + j-1 ] - 2 * pic[ xsize * (i+1) + j ] - pic[ xsize * (i+1) + j+1 ];
    
    magnitude =  sum1*sum1 + sum2*sum2;
    
    if(offset < (xsize-2)*(ysize-2)) {
        result[picindex] = 255*(magnitude > thresh);
        
        //Color in the left, right, top, botom, sides black
        if(i == 1)       result[(i-1)*xsize + j] = 0;
        if(i == ysize-2) result[(i+1)*xsize + j] = 0;      
        if(j == 1)       result[i*xsize + (j-1)] = 0;
        if(j == xsize-2) result[i*xsize + (j+1)] = 0;

        //Color in the four corners.
        if(i == 1 && j == 1)             result[(i-1)*xsize + (j-1)] = 0;
        if(i == 1 && j == xsize-2)       result[(i-1)*xsize + (j+1)] = 0;
        if(i == ysize-2 && j == 1)       result[(i+1)*xsize + (j-1)] = 0;
        if(i == ysize-2 && j == xsize-2) result[(i+1)*xsize + (j+1)] = 0;
    }
    

    return;
}

/*
Description:
    Similar to the basic global memory algorithm, except we make
    sure to access contiguous memory locations without going back, to save time.
    */
__global__ void sobel_parallel_cuda_2(unsigned int* pic, unsigned int* result, int xsize, int ysize, int thresh) {

    int i, j, sum1, sum2, magnitude;    
    int offset = blockIdx.x*blockDim.x+threadIdx.x;
    i = offset / (xsize-2) + 1;
    j = offset % (xsize-2) + 1;
    int picindex = i*xsize + j;

    sum1 = 0;
    sum2 = 0;
    
    sum1 +=   -pic[xsize*(i-1) + j-1];
    sum2 +=    pic[xsize*(i-1) + j-1];   
    sum2 +=  2*pic[xsize*(i-1) + j  ];  
    sum1 +=    pic[xsize*(i-1) + j+1];
    sum2 +=    pic[xsize*(i-1) + j+1];

    sum1 += -2*pic[xsize*(i)   + j-1];
    sum1 +=  2*pic[xsize*(i)   + j+1];
    
    sum1 +=   -pic[xsize*(i+1) + j-1];
    sum2 +=   -pic[xsize*(i+1) + j-1];
    sum2 += -2*pic[xsize*(i+1) + j  ];
    sum1 +=    pic[xsize*(i+1) + j+1];
    sum2 +=   -pic[xsize*(i+1) + j+1];
    
    magnitude =  sum1*sum1 + sum2*sum2;
    
    if(offset < (xsize-2)*(ysize-2)) {
        result[picindex] = 255*(magnitude > thresh);
        
        //Color in the left, right, top, botom, sides black
        if(i == 1)       result[(i-1)*xsize + j] = 0;
        if(i == ysize-2) result[(i+1)*xsize + j] = 0;      
        if(j == 1)       result[i*xsize + (j-1)] = 0;
        if(j == xsize-2) result[i*xsize + (j+1)] = 0;

        //Color in the four corners.
        if(i == 1 && j == 1)             result[(i-1)*xsize + (j-1)] = 0;
        if(i == 1 && j == xsize-2)       result[(i-1)*xsize + (j+1)] = 0;
        if(i == ysize-2 && j == 1)       result[(i+1)*xsize + (j-1)] = 0;
        if(i == ysize-2 && j == xsize-2) result[(i+1)*xsize + (j+1)] = 0;
    }

    return;
}


/*
Description:
    We're going to have each thread given a vertical column of the image.
    All threads do one line of the image at a time, each thread on a pixel (one per column)
    and it will move down the image top-to-bottom in lock-step.
    I'm also breaking up the image vertically, since that might have speed improvements to do that too.  But I'm not sure yet.
    */
__global__ void sobel_parallel_opt_1(unsigned int* pic,
                                    unsigned int* result, 
                                    int xsize,
                                    int ysize,
                                    int thresh,
                                    int ysplitsize,
                                    int blocksize,
                                    int numblocksx) {

    int i, j, sum1, sum2, magnitude;    
    int offset = blockIdx.x*blockDim.x+threadIdx.x;

    i = (offset / (blocksize*numblocksx))*ysplitsize + 1;  //row
    j = offset % (blocksize*numblocksx) + 1; //column
    int row_end = i + ysplitsize;
    
    if( i >= 1 && i < ysize-1 && j >= 1 && j < xsize-1 ) {

        //Saving the middle and bottom rows for next iteration
        int pixel_00, pixel_01, pixel_02;
        int pixel_10, pixel_11, pixel_12;        
        int picindex = 0;

        sum1 = 0;
        sum2 = 0;

        //Top row.
        sum1 +=   -pic[xsize*(i-1) + j-1];
        sum2 +=    pic[xsize*(i-1) + j-1];   
        sum2 +=  2*pic[xsize*(i-1) + j  ];  
        sum1 +=    pic[xsize*(i-1) + j+1];
        sum2 +=    pic[xsize*(i-1) + j+1];

        //Save middle row for next iteration
        pixel_00 = pic[xsize*(i)   + j-1];
        pixel_01 = pic[xsize*(i)   + j  ];
        pixel_02 = pic[xsize*(i)   + j+1];
        
        // Middle Row
        sum1 += -2*pixel_00;
        sum1 +=  2*pixel_02;

        //Save bottom row for next iteration
        pixel_10 = pic[xsize*(i+1) + j-1];
        pixel_11 = pic[xsize*(i+1) + j  ];
        pixel_12 = pic[xsize*(i+1) + j+1];
    
        //Bottom row
        sum1 +=   -pixel_10;
        sum2 +=   -pixel_10;
        sum2 += -2*pixel_11;
        sum1 +=    pixel_12;
        sum2 +=   -pixel_12;
        
        magnitude =  sum1*sum1 + sum2*sum2;
    
        //Color in the pixel appropriately
        picindex = i*xsize + j;
        result[picindex] = 255*(magnitude > thresh);

        //Color in the left, right, top, botom, sides black
        if(i == 1)       result[(i-1)*xsize + j] = 0;
        if(i == ysize-2) result[(i+1)*xsize + j] = 0;      
        if(j == 1)       result[i*xsize + (j-1)] = 0;
        if(j == xsize-2) result[i*xsize + (j+1)] = 0;

        //Color in the four corners.
        if(i == 1 && j == 1)             result[(i-1)*xsize + (j-1)] = 0;
        if(i == 1 && j == xsize-2)       result[(i-1)*xsize + (j+1)] = 0;
        if(i == ysize-2 && j == 1)       result[(i+1)*xsize + (j-1)] = 0;
        if(i == ysize-2 && j == xsize-2) result[(i+1)*xsize + (j+1)] = 0;

        //Now, we do the other rows, using the saved values to minimize memory access:
        i++;
        for(; i < row_end && i < ysize-1; i++ ) {    
            sum1 = 0;
            sum2 = 0;
    
            //Top row
            sum1 +=   -pixel_00;
            sum2 +=    pixel_00;   
            sum2 +=  2*pixel_01;  
            sum1 +=    pixel_02;
            sum2 +=    pixel_02;

            //Middle row
            sum1 += -2*pixel_10;
            sum1 +=  2*pixel_12;
    
            //Set top row to middle row values for next iteration
            pixel_00 = pixel_10;
            pixel_01 = pixel_11;
            pixel_02 = pixel_12;
        
            //Load bottom row values: set middle row values to bottom row values for next iteration
            pixel_10 = pic[xsize*(i+1) + j-1];
            pixel_11 = pic[xsize*(i+1) + j  ];
            pixel_12 = pic[xsize*(i+1) + j+1];
    
            //Bottom row
            sum1 +=   -pixel_10;
            sum2 +=   -pixel_10;
            sum2 += -2*pixel_11;
            sum1 +=    pixel_12;
            sum2 +=   -pixel_12;
    
            magnitude =  sum1*sum1 + sum2*sum2;
    
            //Color in the pixel appropriately
            picindex = i*xsize + j;
            result[picindex] = 255*(magnitude > thresh);

            //Color in the left, right, top, botom, sides black
            if(i == 1)       result[(i-1)*xsize + j] = 0;
            if(i == ysize-2) result[(i+1)*xsize + j] = 0;      
            if(j == 1)       result[i*xsize + (j-1)] = 0;
            if(j == xsize-2) result[i*xsize + (j+1)] = 0;

            //Color in the four corners.
            if(i == 1 && j == 1)             result[(i-1)*xsize + (j-1)] = 0;
            if(i == 1 && j == xsize-2)       result[(i-1)*xsize + (j+1)] = 0;
            if(i == ysize-2 && j == 1)       result[(i+1)*xsize + (j-1)] = 0;
            if(i == ysize-2 && j == xsize-2) result[(i+1)*xsize + (j+1)] = 0;

        }
    }
    return;
}


/*
Description:
    SAME AS "opt_1", except we took out the "pixel_OO" variables, to
    see if it changes the speed (seems to me it should make it slower)
    */
__global__ void sobel_parallel_opt_2(unsigned int* pic,
                                    unsigned int* result, 
                                    int xsize,
                                    int ysize,
                                    int thresh,
                                    int ysplitsize,
                                    int blocksize,
                                    int numblocksx) {

    int i, j, sum1, sum2, magnitude;    
    int offset = blockIdx.x*blockDim.x+threadIdx.x;

    i = (offset / (blocksize*numblocksx))*ysplitsize + 1;  //row
    j = offset % (blocksize*numblocksx) + 1; //column
    int row_end = i + ysplitsize;
    
    int picindex = 0;
    if( i >= 1 && i < ysize-1 && j >= 1 && j < xsize-1 ) {
        for(; i < row_end && i < ysize-1; i++ ) {

            sum1 = 0;
            sum2 = 0;
    
            //Top row
            sum1 +=   -pic[xsize*(i-1) + j-1];
            sum2 +=    pic[xsize*(i-1) + j-1];   
            sum2 +=  2*pic[xsize*(i-1) + j  ];  
            sum1 +=    pic[xsize*(i-1) + j+1];
            sum2 +=    pic[xsize*(i-1) + j+1];

            //Middle row
            sum1 += -2*pic[xsize*(i)   + j-1];
            sum1 +=  2*pic[xsize*(i)   + j+1];
    
            //Bottom row
            sum1 +=   -pic[xsize*(i+1) + j-1];
            sum2 +=   -pic[xsize*(i+1) + j-1];
            sum2 += -2*pic[xsize*(i+1) + j  ];
            sum1 +=    pic[xsize*(i+1) + j+1];
            sum2 +=   -pic[xsize*(i+1) + j+1];
        
            magnitude =  sum1*sum1 + sum2*sum2;
    
            //Color in the pixel appropriately
            picindex = i*xsize + j;
            result[picindex] = 255*(magnitude > thresh);

            //Color in the left, right, top, botom, sides black
            if(i == 1)       result[(i-1)*xsize + j] = 0;
            if(i == ysize-2) result[(i+1)*xsize + j] = 0;      
            if(j == 1)       result[i*xsize + (j-1)] = 0;
            if(j == xsize-2) result[i*xsize + (j+1)] = 0;

            //Color in the four corners.
            if(i == 1 && j == 1)             result[(i-1)*xsize + (j-1)] = 0;
            if(i == 1 && j == xsize-2)       result[(i-1)*xsize + (j+1)] = 0;
            if(i == ysize-2 && j == 1)       result[(i+1)*xsize + (j-1)] = 0;
            if(i == ysize-2 && j == xsize-2) result[(i+1)*xsize + (j+1)] = 0;
        }
    }
}


/*
Description: SAME AS "opt_1", except using "Unroll and Jam"
    We're going to have each thread given THREE vertical column of the image.
    All threads do one line of the image at a time, each thread does THREE pixels (three columns)
    and it will move down the image top-to-bottom in lock-step.
    I'm also breaking up the image vertically, since that might have speed improvements to do that too.  But I'm not sure yet.
    */
__global__ void sobel_parallel_opt_3(unsigned int* pic,
                                    unsigned int* result, 
                                    int xsize,
                                    int ysize,
                                    int thresh,
                                    int ysplitsize,
                                    int blocksize,
                                    int numblocksx) {

    int i, j, sum1a, sum2a, sum1b, sum2b, sum1c, sum2c, magnitude_a, magnitude_b, magnitude_c;    
    int offset = blockIdx.x*blockDim.x+threadIdx.x;

    i = (offset / (blocksize*numblocksx))*ysplitsize + 1;  //row
    j = 3*(offset % (blocksize*numblocksx)) + 1; //column
    int row_end = i + ysplitsize;
    
    if( i >= 1 && i < ysize-1 && j >= 1 && j < xsize-1 ) {

        //Saving the middle and bottom rows for next iteration
        int pixel_00, pixel_01, pixel_02, pixel_03, pixel_04;
        int pixel_10, pixel_11, pixel_12, pixel_13, pixel_14;

        sum1a = 0;
        sum1b = 0;
        sum1c = 0;
        sum2a = 0;
        sum2b = 0;
        sum2c = 0;
        
        //Top row
        sum1a +=   -pic[xsize*(i-1) + j-1];
        sum1b +=   -pic[xsize*(i-1) + j];
        sum1c +=   -pic[xsize*(i-1) + j+1];
        //^^^
        sum2a +=    pic[xsize*(i-1) + j-1];
        sum2b +=    pic[xsize*(i-1) + j];
        sum2c +=    pic[xsize*(i-1) + j+1];
        //^^^   
        sum2a +=  2*pic[xsize*(i-1) + j  ];
        sum2b +=  2*pic[xsize*(i-1) + j+1  ];
        sum2c +=  2*pic[xsize*(i-1) + j+2  ];
        //^^^  
        sum1a +=    pic[xsize*(i-1) + j+1];
        sum1b +=    pic[xsize*(i-1) + j+2];
        sum1c +=    pic[xsize*(i-1) + j+3];
        //^^^
        sum2a +=    pic[xsize*(i-1) + j+1];
        sum2b +=    pic[xsize*(i-1) + j+2];
        sum2c +=    pic[xsize*(i-1) + j+3];

        //Save middle values for next iteration
        pixel_00 = pic[xsize*(i)   + j-1];
        pixel_01 = pic[xsize*(i)   + j  ];
        pixel_02 = pic[xsize*(i)   + j+1];
        pixel_03 = pic[xsize*(i)   + j+2];
        pixel_04 = pic[xsize*(i)   + j+3];
        
        //Midle row
        sum1a += -2*pixel_00;
        sum1b += -2*pixel_01;
        sum1c += -2*pixel_02;
        //^^^
        sum1a +=  2*pixel_02;
        sum1b +=  2*pixel_03;
        sum1c +=  2*pixel_04;

        //Save bottom values for next iteration
        pixel_10 = pic[xsize*(i+1) + j-1];
        pixel_11 = pic[xsize*(i+1) + j  ];
        pixel_12 = pic[xsize*(i+1) + j+1];
        pixel_13 = pic[xsize*(i+1) + j+2];
        pixel_14 = pic[xsize*(i+1) + j+3];
    
        //Bottom row
        sum1a +=   -pixel_10;
        sum1b +=   -pixel_11;
        sum1c +=   -pixel_12;
        //^^^
        sum2a +=   -pixel_10;
        sum2b +=   -pixel_11;
        sum2c +=   -pixel_12;
        //^^^
        sum2a += -2*pixel_11;
        sum2b += -2*pixel_12;
        sum2c += -2*pixel_13;
        //^^^
        sum1a +=    pixel_12;
        sum1b +=    pixel_13;
        sum1c +=    pixel_14;
        //^^^
        sum2a +=   -pixel_12;
        sum2b +=   -pixel_13;
        sum2c +=   -pixel_14;
        
        magnitude_a =  sum1a*sum1a + sum2a*sum2a;
        magnitude_b =  sum1b*sum1b + sum2b*sum2b;
        magnitude_c =  sum1c*sum1c + sum2c*sum2c;
    
        //Color in the pixel appropriately
        if(j <= xsize-2) {
            result[i*xsize + j] = 255*(magnitude_a > thresh);
        }
        if(j+1 <= xsize-2) {
            result[i*xsize + j+1] = 255*(magnitude_b > thresh);
        }
        if(j+2 <= xsize-2) {
            result[i*xsize + j+2] = 255*(magnitude_c > thresh);
        }

        //Color in the left, right, top, botom, sides black
        if(i == 1) {
            //top
            if(j <= xsize-2) result[(i-1)*xsize + j] = 0;
            if(j+1 <= xsize-2) result[(i-1)*xsize + j+1] = 0;
            if(j+2 <= xsize-2) result[(i-1)*xsize + j+2] = 0;
        }
        if(i == ysize-2) {
            //bottom
            if(j <= xsize-2)  result[(i+1)*xsize + j] = 0;
            if(j+1 <= xsize-2)  result[(i+1)*xsize + j+1] = 0;
            if(j+2 <= xsize-2)  result[(i+1)*xsize + j+2] = 0;
        }      
        if(j == 1) {
            //Left
            result[i*xsize + (j-1)] = 0;
        }
        ////Right
        if(j == xsize-2) {
            result[i*xsize + (j+1)] = 0;
        }
        if(j+1 == xsize-2) {
            result[i*xsize + (j+2)] = 0;
        }
        if(j+2 == xsize-2) {
            result[i*xsize + (j+3)] = 0;
        }

        //Color in the four corners.
        //Top left
        if(i == 1 && j == 1)               result[(i-1)*xsize + (j-1)] = 0;
        //Top Right
        if(i == 1 && j == xsize-2)         result[(i-1)*xsize + (j+1)] = 0;
        if(i == 1 && j+1 == xsize-2)       result[(i-1)*xsize + (j+2)] = 0;
        if(i == 1 && j+2 == xsize-2)       result[(i-1)*xsize + (j+3)] = 0;
        //Bottom Left
        if(i == ysize-2 && j == 1)         result[(i+1)*xsize + (j-1)] = 0;
        //Bottom Right
        if(i == ysize-2 && j == xsize-2)   result[(i+1)*xsize + (j+1)] = 0;
        if(i == ysize-2 && j+1 == xsize-2) result[(i+1)*xsize + (j+2)] = 0;
        if(i == ysize-2 && j+2 == xsize-2) result[(i+1)*xsize + (j+3)] = 0;


        //Now, we do the other rows, using the saved values to minimize memory access:
        i++;
        for(; i < row_end && i < ysize-1; i++ ) {    
            sum1a = 0;
            sum1b = 0;
            sum1c = 0;
            sum2a = 0;
            sum2b = 0;
            sum2c = 0;
    
            //Top row
            sum1a +=   -pixel_00;
            sum1b +=   -pixel_01;
            sum1c +=   -pixel_02;
            //^^^
            sum2a +=    pixel_00;
            sum2b +=    pixel_01;
            sum2c +=    pixel_02;
            //^^^   
            sum2a +=  2*pixel_01;
            sum2b +=  2*pixel_02;
            sum2c +=  2*pixel_03; 
            //^^^ 
            sum1a +=    pixel_02;
            sum1b +=    pixel_03;
            sum1c +=    pixel_04;
            //^^^
            sum2a +=    pixel_02;
            sum2b +=    pixel_03;
            sum2c +=    pixel_04;

            //Middle row
            sum1a += -2*pixel_10;
            sum1b += -2*pixel_11;
            sum1c += -2*pixel_12;
            //^^^
            sum1a +=  2*pixel_12;
            sum1b +=  2*pixel_13;
            sum1c +=  2*pixel_14;
    
            //Set top row to middle row values for next iteration
            pixel_00 = pixel_10;
            pixel_01 = pixel_11;
            pixel_02 = pixel_12;
            pixel_03 = pixel_13;
            pixel_04 = pixel_14;
        
            //Load bottom row values: set middle row values to bottom row values for next iteration
            pixel_10 = pic[xsize*(i+1) + j-1];
            pixel_11 = pic[xsize*(i+1) + j  ];
            pixel_12 = pic[xsize*(i+1) + j+1];
            pixel_13 = pic[xsize*(i+1) + j+2];
            pixel_14 = pic[xsize*(i+1) + j+3];
    
            //Bottom row
            sum1a +=   -pixel_10;
            sum1b +=   -pixel_11;
            sum1c +=   -pixel_12;
            //^^^
            sum2a +=   -pixel_10;
            sum2b +=   -pixel_11;
            sum2c +=   -pixel_12;
            //^^^
            sum2a += -2*pixel_11;
            sum2b += -2*pixel_12;
            sum2c += -2*pixel_13;
            //^^^
            sum1a +=    pixel_12;
            sum1b +=    pixel_13;
            sum1c +=    pixel_14;
            //^^^
            sum2a +=   -pixel_12;
            sum2b +=   -pixel_13;
            sum2c +=   -pixel_14;
    
            magnitude_a =  sum1a*sum1a + sum2a*sum2a;
            magnitude_b =  sum1b*sum1b + sum2b*sum2b;
            magnitude_c =  sum1c*sum1c + sum2c*sum2c;
    
            //Color in the pixel appropriately
            if(j <= xsize-2) {
                result[i*xsize + j] = 255*(magnitude_a > thresh);
            }
            if(j+1 <= xsize-2) {
                result[i*xsize + j+1] = 255*(magnitude_b > thresh);
            }
            if(j+2 <= xsize-2) {
                result[i*xsize + j+2] = 255*(magnitude_c > thresh);
            }

            //Color in the left, right, top, botom, sides black
            if(i == 1) {
                //top
                if(j <= xsize-2) result[(i-1)*xsize + j] = 0;
                if(j+1 <= xsize-2) result[(i-1)*xsize + j+1] = 0;
                if(j+2 <= xsize-2) result[(i-1)*xsize + j+2] = 0;
            }
            if(i == ysize-2) {
                //bottom
                if(j <= xsize-2)  result[(i+1)*xsize + j] = 0;
                if(j+1 <= xsize-2)  result[(i+1)*xsize + j+1] = 0;
                if(j+2 <= xsize-2)  result[(i+1)*xsize + j+2] = 0;
            }      
            if(j == 1) {
                //Left
                result[i*xsize + (j-1)] = 0;
            }
            ////Right
            if(j == xsize-2) {
                result[i*xsize + (j+1)] = 0;
            }
            if(j+1 == xsize-2) {
                result[i*xsize + (j+2)] = 0;
            }
            if(j+2 == xsize-2) {
                result[i*xsize + (j+3)] = 0;
            }

            //Color in the four corners.
            //Top left
            if(i == 1 && j == 1)               result[(i-1)*xsize + (j-1)] = 0;
            //Top Right
            if(i == 1 && j == xsize-2)         result[(i-1)*xsize + (j+1)] = 0;
            if(i == 1 && j+1 == xsize-2)       result[(i-1)*xsize + (j+2)] = 0;
            if(i == 1 && j+2 == xsize-2)       result[(i-1)*xsize + (j+3)] = 0;
            //Bottom Left
            if(i == ysize-2 && j == 1)         result[(i+1)*xsize + (j-1)] = 0;
            //Bottom Right
            if(i == ysize-2 && j == xsize-2)   result[(i+1)*xsize + (j+1)] = 0;
            if(i == ysize-2 && j+1 == xsize-2) result[(i+1)*xsize + (j+2)] = 0;
            if(i == ysize-2 && j+2 == xsize-2) result[(i+1)*xsize + (j+3)] = 0;

        }
    }
    return;
}


/*Description:
           Each block copies a section from the photo to shared memory
           Overlap so that you know the edge just off of what you save.
           Read from shared memory and save to global.
*/
#define OPT_4_BLOCK_DIM_Y 16
#define OPT_4_BLOCK_DIM_X 16

__global__ void sobel_parallel_opt_4(unsigned int* pic, unsigned int* result, int xsize, int ysize, int thresh) {

    __shared__ int shpic[OPT_4_BLOCK_DIM_Y][OPT_4_BLOCK_DIM_X+1];

    int i, j, pic_i, pic_j, sum1, sum2, magnitude;
    pic_i = blockIdx.y*blockDim.y + threadIdx.y - 2*blockIdx.y;
    pic_j = blockIdx.x*blockDim.x + threadIdx.x - 2*blockIdx.x;
    int picindex = pic_i*xsize + pic_j;
    i = threadIdx.y;
    j = threadIdx.x;
    
    if( pic_i >= 0 && pic_i < ysize && pic_j >= 0 && pic_j < xsize) {
        shpic[i][j] = pic[picindex];
    }
    
    __syncthreads();
    
    
    if( pic_i >= 1 && pic_i <= ysize-2  && pic_j >= 1 && pic_j <= xsize-2
         && i >= 1 &&     i <= blockDim.y-2 && j >= 1     && j <= blockDim.x-2) {
         
        sum1 = 0;
        sum2 = 0;
    
        sum1 +=   -shpic[i-1][j-1];
        sum2 +=    shpic[i-1][j-1];   
        sum2 +=  2*shpic[i-1][j  ];  
        sum1 +=    shpic[i-1][j+1];
        sum2 +=    shpic[i-1][j+1];

        sum1 += -2*shpic[i][j-1];
        sum1 +=  2*shpic[i][j+1];
    
        sum1 +=   -shpic[i+1][j-1];
        sum2 +=   -shpic[i+1][j-1];
        sum2 += -2*shpic[i+1][j  ];
        sum1 +=    shpic[i+1][j+1];
        sum2 +=   -shpic[i+1][j+1];
    
        magnitude =  sum1*sum1 + sum2*sum2;

        result[picindex] = 255*(magnitude > thresh);
        //result[picindex] = shpic[i][j];
        
        i = pic_i;
        j = pic_j;
        //Color in the left, right, top, botom, sides black
        if(i == 1)       result[(i-1)*xsize + j] = 0;
        if(i == ysize-2) result[(i+1)*xsize + j] = 0;      
        if(j == 1)       result[i*xsize + (j-1)] = 0;
        if(j == xsize-2) result[i*xsize + (j+1)] = 0;

        //Color in the four corners.
        if(i == 1 && j == 1)             result[(i-1)*xsize + (j-1)] = 0;
        if(i == 1 && j == xsize-2)       result[(i-1)*xsize + (j+1)] = 0;
        if(i == ysize-2 && j == 1)       result[(i+1)*xsize + (j-1)] = 0;
        if(i == ysize-2 && j == xsize-2) result[(i+1)*xsize + (j+1)] = 0;

    }
    return;
}



//*********************
//*********************
//*********************
//*********************
//*********************


extern "C" void sobel_cpu_1(unsigned int* pic, unsigned int* result, int xsize, int ysize, int thresh) {

    /*
    * Performing the function on the CPU
    */
    fprintf(stderr,"sobel_algorithm_cpu_1...\n");

    //Timer start
    cudaEvent_t start_event_outer, stop_event_outer, start_event, stop_event;
    float elapsed_time_outer, elapsed_time_inner;
    cudaEventCreate(&start_event_outer);
    cudaEventCreate(&stop_event_outer);
    cudaEventRecord(start_event_outer, 0);  
    
    {
        cudaEventCreate(&start_event);
        cudaEventCreate(&stop_event);
        cudaEventRecord(start_event, 0);  
    
        sobel_algorithm_cpu_1(pic, result, xsize, ysize, thresh);
    
        cudaEventRecord(stop_event, 0);
        cudaEventSynchronize(stop_event);
        cudaEventElapsedTime(&elapsed_time_inner, start_event, stop_event);
    }
        
    cudaEventRecord(stop_event_outer, 0);
    cudaEventSynchronize(stop_event_outer);
    cudaEventElapsedTime(&elapsed_time_outer, start_event_outer, stop_event_outer);
    fprintf(stderr,"INNER: %.8f\n", elapsed_time_inner);
    fprintf(stderr,"OUTER: %.8f\n",elapsed_time_outer);
            
    return;
}




extern "C" void sobel_cpu_2(unsigned int* pic, unsigned int* result, int xsize, int ysize, int thresh) {

    /*
    * Performing the function on the CPU
    */
    fprintf(stderr,"sobel_algorithm_cpu_2...\n");

    //Timer start
    cudaEvent_t start_event_outer, stop_event_outer, start_event, stop_event;
    float elapsed_time_outer, elapsed_time_inner;
    cudaEventCreate(&start_event_outer);
    cudaEventCreate(&stop_event_outer);
    cudaEventRecord(start_event_outer, 0);  

    {
        cudaEventCreate(&start_event);
        cudaEventCreate(&stop_event);
        cudaEventRecord(start_event, 0);  

        sobel_algorithm_cpu_2(pic, result, xsize, ysize, thresh);

        cudaEventRecord(stop_event, 0);
        cudaEventSynchronize(stop_event);
        cudaEventElapsedTime(&elapsed_time_inner, start_event, stop_event);
    }
            
    cudaEventRecord(stop_event_outer, 0);
    cudaEventSynchronize(stop_event_outer);
    cudaEventElapsedTime(&elapsed_time_outer, start_event_outer, stop_event_outer);
    fprintf(stderr,"INNER: %.8f\n", elapsed_time_inner);
    fprintf(stderr,"OUTER: %.8f\n",elapsed_time_outer);
    
    return;

}




extern "C" void sobel_cuda_1(unsigned int* pic, unsigned int* result, int xsize, int ysize, int thresh) {

    /*
    * Performing the function on the CUDA device.
    */
    fprintf(stderr,"sobel_parallel_cuda_1...\n");

    //Timer start
    cudaEvent_t start_event_outer, stop_event_outer, start_event, stop_event;
    float elapsed_time_outer, elapsed_time_inner;
    cudaEventCreate(&start_event_outer);
    cudaEventCreate(&stop_event_outer);
    cudaEventRecord(start_event_outer, 0);  
    
    //The amoount of memory that one array takes up
    int nbytes = xsize * ysize * sizeof( unsigned int );
    
    //Allocating the memory on the CUDA device:
    unsigned int *d_pic;
    unsigned int *d_result;
    cudaMalloc(&d_pic, nbytes);
    cudaMalloc(&d_result, nbytes);
    
    //Copying arrays to the CUDA device:
    cudaMemcpy( d_pic, pic, nbytes, cudaMemcpyHostToDevice);
    //cudaMemcpy( d_result, result, nbytes, cudaMemcpyHostToDevice);

    {
        int sz = (xsize-2)*(ysize-2);
        int blocksize = 1024;
        int gridsize = (int)ceil((float)sz/(float)blocksize);

        cudaEventCreate(&start_event);
        cudaEventCreate(&stop_event);
        cudaEventRecord(start_event, 0);  

        // Performing the calculation on the CUDA device
        sobel_parallel_cuda_1<<<gridsize,blocksize>>>(d_pic,d_result,xsize,ysize,thresh);

        cudaEventRecord(stop_event, 0);
        cudaEventSynchronize(stop_event);
        cudaEventElapsedTime(&elapsed_time_inner, start_event, stop_event);
    }        
	
	//Copying the arrays back to the host (the regular computer)
	cudaMemcpy(result, d_result, nbytes, cudaMemcpyDeviceToHost);
	
	cudaFree(d_pic);
	cudaFree(d_result);    

    cudaEventRecord(stop_event_outer, 0);
    cudaEventSynchronize(stop_event_outer);
    cudaEventElapsedTime(&elapsed_time_outer, start_event_outer, stop_event_outer);
    fprintf(stderr,"INNER: %.8f\n", elapsed_time_inner);
    fprintf(stderr,"OUTER: %.8f\n",elapsed_time_outer);

	return;
}



extern "C" void sobel_cuda_2(unsigned int* pic, unsigned int* result, int xsize, int ysize, int thresh) {

    /*
    * Performing the function on the CUDA device.
    */
    fprintf(stderr,"sobel_parallel_cuda_2...\n");

    //Timer start
    cudaEvent_t start_event_outer, stop_event_outer, start_event, stop_event;
    float elapsed_time_outer, elapsed_time_inner;
    cudaEventCreate(&start_event_outer);
    cudaEventCreate(&stop_event_outer);
    cudaEventRecord(start_event_outer, 0);  
    
    //The amoount of memory that one array takes up
    int nbytes = xsize * ysize * sizeof( unsigned int );
    
    //Allocating the memory on the CUDA device:
    unsigned int *d_pic;
    unsigned int *d_result;
    cudaMalloc(&d_pic, nbytes);
    cudaMalloc(&d_result, nbytes);
    
    //Copying arrays to the CUDA device:
    cudaMemcpy( d_pic, pic, nbytes, cudaMemcpyHostToDevice);
    //cudaMemcpy( d_result, result, nbytes, cudaMemcpyHostToDevice);

    {
        int sz = (xsize-2)*(ysize-2);
        int blocksize = 1024;
        int gridsize = (int)ceil((float)sz/(float)blocksize);
    
        cudaEventCreate(&start_event);
        cudaEventCreate(&stop_event);
        cudaEventRecord(start_event, 0); 

        // Performing the calculation on the CUDA device
        sobel_parallel_cuda_2<<<gridsize,blocksize>>>(d_pic,d_result,xsize,ysize,thresh);

        cudaEventRecord(stop_event, 0);
        cudaEventSynchronize(stop_event);
        cudaEventElapsedTime(&elapsed_time_inner, start_event, stop_event);
    }

	//Copying the arrays back to the host (the regular computer)
	cudaMemcpy(result, d_result, nbytes, cudaMemcpyDeviceToHost);
	
	cudaFree(d_pic);
	cudaFree(d_result);    

    cudaEventRecord(stop_event_outer, 0);
    cudaEventSynchronize(stop_event_outer);
    cudaEventElapsedTime(&elapsed_time_outer, start_event_outer, stop_event_outer);
    fprintf(stderr,"INNER: %.8f\n", elapsed_time_inner);
    fprintf(stderr,"OUTER: %.8f\n",elapsed_time_outer);

	return;
}





extern "C" void sobel_opt_1(unsigned int* pic, unsigned int* result, int xsize, int ysize, int thresh) {

    /*
    * Performing the function on the CUDA device.
    */
    fprintf(stderr,"sobel_parallel_opt_1...\n");

    //Timer start
    cudaEvent_t start_event_outer, stop_event_outer, start_event, stop_event;
    float elapsed_time_outer, elapsed_time_inner;
    cudaEventCreate(&start_event_outer);
    cudaEventCreate(&stop_event_outer);
    cudaEventRecord(start_event_outer, 0);  
    
    //The amoount of memory that one array takes up
    int nbytes = xsize * ysize * sizeof( unsigned int );
    
    //Allocating the memory on the CUDA device:
    unsigned int *d_pic;
    unsigned int *d_result;
    cudaMalloc(&d_pic, nbytes);
    cudaMalloc(&d_result, nbytes);
    
    //Copying arrays to the CUDA device:
    cudaMemcpy( d_pic, pic, nbytes, cudaMemcpyHostToDevice);
    //cudaMemcpy( d_result, result, nbytes, cudaMemcpyHostToDevice);
       
    {
        int blocksize = 32;
        int ysplitsize = 8;
        int numblocksx = (int)ceil((float)(xsize-2)/(float)blocksize);
        int gridsize = numblocksx * (int)ceil((float)(ysize-2)/(float)ysplitsize);

        cudaEventCreate(&start_event);
        cudaEventCreate(&stop_event);
        cudaEventRecord(start_event, 0); 

        // Performing the calculation on the CUDA device
        sobel_parallel_opt_1<<<gridsize,blocksize>>>(d_pic,d_result,xsize,ysize,thresh,ysplitsize,blocksize,numblocksx);

        cudaEventRecord(stop_event, 0);
        cudaEventSynchronize(stop_event);
        cudaEventElapsedTime(&elapsed_time_inner, start_event, stop_event);
    }

	//Copying the arrays back to the host (the regular computer)
	cudaMemcpy(result, d_result, nbytes, cudaMemcpyDeviceToHost);
	
	cudaFree(d_pic);
	cudaFree(d_result);    

    cudaEventRecord(stop_event_outer, 0);
    cudaEventSynchronize(stop_event_outer);
    cudaEventElapsedTime(&elapsed_time_outer, start_event_outer, stop_event_outer);
    fprintf(stderr,"INNER: %.8f\n", elapsed_time_inner);
    fprintf(stderr,"OUTER: %.8f\n",elapsed_time_outer);

	return;
}










extern "C" void sobel_opt_2(unsigned int* pic, unsigned int* result, int xsize, int ysize, int thresh) {
    
    /*
    * Performing the function on the CUDA device.
    */
    fprintf(stderr,"sobel_parallel_opt_2...\n");


    //Timer start
    cudaEvent_t start_event_outer, stop_event_outer, start_event, stop_event;
    float elapsed_time_outer, elapsed_time_inner;
    cudaEventCreate(&start_event_outer);
    cudaEventCreate(&stop_event_outer);
    cudaEventRecord(start_event_outer, 0);  

    
    //The amoount of memory that one array takes up
    int nbytes = xsize * ysize * sizeof( unsigned int );
    
    //Allocating the memory on the CUDA device:
    unsigned int *d_pic;
    unsigned int *d_result;
    cudaMalloc(&d_pic, nbytes);
    cudaMalloc(&d_result, nbytes);
    
    //Copying arrays to the CUDA device:
    cudaMemcpy( d_pic, pic, nbytes, cudaMemcpyHostToDevice);
    //cudaMemcpy( d_result, result, nbytes, cudaMemcpyHostToDevice);
    
    //Calculating the grid size (a.k.a. the number of blocks)
   
        {      
            int blocksize = 32;
            int ysplitsize = 8;
            int numblocksx = (int)ceil((float)(xsize-2)/(float)blocksize);
            int gridsize = numblocksx * (int)ceil((float)(ysize-2)/(float)ysplitsize);

            cudaEventCreate(&start_event);
            cudaEventCreate(&stop_event);
            cudaEventRecord(start_event, 0); 

            // Performing the calculation on the CUDA device
            sobel_parallel_opt_2<<<gridsize,blocksize>>>(d_pic,d_result,xsize,ysize,thresh,ysplitsize,blocksize,numblocksx);
    
            cudaEventRecord(stop_event, 0);
            cudaEventSynchronize(stop_event);
            cudaEventElapsedTime(&elapsed_time_inner, start_event, stop_event);
        }
           
	//Copying the arrays back to the host (the regular computer)
	cudaMemcpy(result, d_result, nbytes, cudaMemcpyDeviceToHost);
	
	cudaFree(d_pic);
	cudaFree(d_result);    

    cudaEventRecord(stop_event_outer, 0);
    cudaEventSynchronize(stop_event_outer);
    cudaEventElapsedTime(&elapsed_time_outer, start_event_outer, stop_event_outer);
    fprintf(stderr,"INNER: %.8f\n", elapsed_time_inner);
    fprintf(stderr,"OUTER: %.8f\n",elapsed_time_outer);

	return;
}



extern "C" void sobel_opt_3(unsigned int* pic, unsigned int* result, int xsize, int ysize, int thresh) {

    /*
    * Performing the function on the CUDA device.
    */
    fprintf(stderr,"sobel_parallel_opt_3...\n");

    //Timer start
    cudaEvent_t start_event_outer, stop_event_outer, start_event, stop_event;
    float elapsed_time_outer, elapsed_time_inner;
    cudaEventCreate(&start_event_outer);
    cudaEventCreate(&stop_event_outer);
    cudaEventRecord(start_event_outer, 0);  

    //The amoount of memory that one array takes up
    int nbytes = xsize * ysize * sizeof( unsigned int );
    
    //Allocating the memory on the CUDA device:
    unsigned int *d_pic;
    unsigned int *d_result;
    cudaMalloc(&d_pic, nbytes);
    cudaMalloc(&d_result, nbytes);
    
    //Copying arrays to the CUDA device:
    cudaMemcpy( d_pic, pic, nbytes, cudaMemcpyHostToDevice);
    //cudaMemcpy( d_result, result, nbytes, cudaMemcpyHostToDevice);
    

    {        
        int blocksize = 32;
        int ysplitsize = 8;
        int numblocksx = (int)ceil((float)(xsize-2)/(float)blocksize*3);
        int gridsize = numblocksx * (int)ceil((float)(ysize-2)/(float)ysplitsize);

        cudaEventCreate(&start_event);
        cudaEventCreate(&stop_event);
        cudaEventRecord(start_event, 0);

        // Performing the calculation on the CUDA device
        sobel_parallel_opt_3<<<gridsize,blocksize>>>(d_pic,d_result,xsize,ysize,thresh,ysplitsize,blocksize,numblocksx);

        cudaEventRecord(stop_event, 0);
        cudaEventSynchronize(stop_event);
        cudaEventElapsedTime(&elapsed_time_inner, start_event, stop_event);
    }

	
	//Copying the arrays back to the host (the regular computer)
	cudaMemcpy(result, d_result, nbytes, cudaMemcpyDeviceToHost);
	
	cudaFree(d_pic);
	cudaFree(d_result);    

    cudaEventRecord(stop_event_outer, 0);
    cudaEventSynchronize(stop_event_outer);
    cudaEventElapsedTime(&elapsed_time_outer, start_event_outer, stop_event_outer);
    fprintf(stderr,"INNER: %.8f\n",elapsed_time_inner);
    fprintf(stderr,"OUTER: %.8f\n",elapsed_time_outer);

	return;
}




extern "C" void sobel_opt_4(unsigned int* pic, unsigned int* result, int xsize, int ysize, int thresh) {

    /*
    * Performing the function on the CUDA device.
    */
    fprintf(stderr,"sobel_parallel_opt_4...\n");
    //fprintf(stderr,"thresh: %d\n", thresh);

    //Timer start
    cudaEvent_t start_event_outer, stop_event_outer, start_event, stop_event;
    float elapsed_time_outer, elapsed_time_inner;
    cudaEventCreate(&start_event_outer);
    cudaEventCreate(&stop_event_outer);
    cudaEventRecord(start_event_outer, 0);  

    //The amoount of memory that one array takes up
    int nbytes = xsize * ysize * sizeof( unsigned int );
    
    //Allocating the memory on the CUDA device:
    unsigned int *d_pic;
    unsigned int *d_result;
    cudaMalloc(&d_pic, nbytes);
    cudaMalloc(&d_result, nbytes);
    
    //Copying arrays to the CUDA device:
    cudaMemcpy( d_pic, pic, nbytes, cudaMemcpyHostToDevice);
    //cudaMemcpy( d_result, result, nbytes, cudaMemcpyHostToDevice);

    {
        int blocksize_x = OPT_4_BLOCK_DIM_X;
        int blocksize_y = OPT_4_BLOCK_DIM_Y;
        int gridsize_x = (int)ceil((float)(xsize-2)/(blocksize_x-2));
        int gridsize_y = (int)ceil((float)(ysize-2)/(blocksize_y-2));
        dim3 blocksize(blocksize_x,blocksize_y,1);
        dim3 gridsize(gridsize_x,gridsize_y,1);

        cudaEventCreate(&start_event);
        cudaEventCreate(&stop_event);
        cudaEventRecord(start_event, 0); 

        // Performing the calculation on the CUDA device
        sobel_parallel_opt_4<<<gridsize,blocksize>>>(d_pic,d_result,xsize,ysize,thresh);

        cudaEventRecord(stop_event, 0);
        cudaEventSynchronize(stop_event);
        cudaEventElapsedTime(&elapsed_time_inner, start_event, stop_event);
    }

	
	//Copying the arrays back to the host (the regular computer)
	cudaMemcpy(result, d_result, nbytes, cudaMemcpyDeviceToHost);
	
	cudaFree(d_pic);
	cudaFree(d_result);    
	
	
    cudaEventRecord(stop_event_outer, 0);
    cudaEventSynchronize(stop_event_outer);
    cudaEventElapsedTime(&elapsed_time_outer, start_event_outer, stop_event_outer);
    fprintf(stderr,"INNER: %.8f\n", elapsed_time_inner);
    fprintf(stderr,"OUTER: %.8f\n",elapsed_time_outer);

	return;
}






extern "C" void sobel_algorithm_ALL(unsigned int* pic, unsigned int* result, int xsize, int ysize, int thresh, int algorithm) {

    switch(algorithm)
    {
        case 0:
            {
                sobel_cpu_1(pic, result, xsize, ysize, thresh);
            }
            break;
        case 1:
            {
                sobel_cpu_2(pic, result, xsize, ysize, thresh);
            }
            break;
        case 2:
            {
                sobel_cuda_1(pic, result, xsize, ysize, thresh);
            }
            break;

        case 3:
            {
                sobel_cuda_2(pic, result, xsize, ysize, thresh);
            }
            break;

        case 4:
            { 
                sobel_opt_1(pic, result, xsize, ysize, thresh);
            }
            break;

        case 5:
            {
                sobel_opt_2(pic, result, xsize, ysize, thresh);
            }
            break;
    
        case 6:
            {
                sobel_opt_3(pic, result, xsize, ysize, thresh);
            }
            break;

        case 7:
            {
                sobel_opt_4(pic, result, xsize, ysize, thresh);
            }
            break;

        default:
            break;
    }
	
	return;
}