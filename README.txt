Steven Schmidt
CS 6235 - Lab 2
Feb 3, 2014

This code runs eight different implementations of the "sobel" algorithm.

Algorithm 0: "sobel_algorithm_cpu_1": Runs serially on the CPU
Algorithm 1: "sobel_algorithm_cpu_2": Runs serially on the CPU. Attempts a slightly altered algorithm to see if it's faster. 
Algorithm 2: "sobel_parallel_cuda_1": Runs in parallel on the GPU.  The most naive: each thread operates on one result pixel.
Algorithm 3: "sobel_parallel_cuda_2": Runs in parallel on the GPU.  Slightly modified, similar to the "cpu_2" modification.
Algorithm 4: "sobel_parallel_opt_1":  Runs in parallel on the GPU.  Each thread works on a column of pixels, saving data in registers to reduce memory accesses
Algorithm 5: "sobel_parallel_opt_2":  Runs in parallel on the GPU.  Same as "opt_1" except without saving to registers (to see how much slower it is).
Algorithm 6: "sobel_parallel_opt_3":  Runs in parallel on the GPU.  Same as "opt_1" except each thread given 3 columns: "unroll and jam".
Algorithm 7: "sobel_parallel_opt_4":  Runs in parallel on the GPU.  Copies input to shared memory, then each thread works on one pixel accessing data from shared array

Each algorithm is timed, with timers measuring the "inner" part and the "outer" part.
"INNER":  The time of just the parallelized "__global__" function.
"OUTER":  The time of both the "__global__" function and the memory copies from host-to-device, and back again.

Input:

There are three input options, which can be switched between by
commentting/uncommentting the appropriate lines at the top of
"sobel-sschmidt.c" and then recompiling.

These input options, and their sizes, are as follows:
(1)
Input:              "BWstop-sign.ppm"
Correct output:     "result_correct.ppm"
Size:               0.2 megapixel

(2)
Input:              "shorttrack.ppm"
Correct output:     "shorttrack_correct.ppm"
Size:               8 megapixels

(3)
Input:              "hurtles.ppm"
Correct output:     "hurtles_correct.ppm"
Size:               14 megapixels

I suggest you test all three inputs -- there is some variation between the algorithms that is only seen with the larger inputs.

To comple:

$ make

To run:

$ ./sobel

To remove everything created in the compilation:

$ make clean