#ifndef SOBEL_SSCHMIDT_CUDA_H
#define SOBEL_SSCHMIDT_CUDA_H


extern void sobel_cpu_1(unsigned int* pic, unsigned int* result, int xsize, int ysize, int thresh);
extern void sobel_cpu_2(unsigned int* pic, unsigned int* result, int xsize, int ysize, int thresh);
extern void sobel_cuda_1(unsigned int* pic, unsigned int* result, int xsize, int ysize, int thresh);
extern void sobel_cuda_2(unsigned int* pic, unsigned int* result, int xsize, int ysize, int thresh);
extern void sobel_opt_1(unsigned int* pic, unsigned int* result, int xsize, int ysize, int thresh);
extern void sobel_opt_2(unsigned int* pic, unsigned int* result, int xsize, int ysize, int thresh);
extern void sobel_opt_3(unsigned int* pic, unsigned int* result, int xsize, int ysize, int thresh);
extern void sobel_opt_4(unsigned int* pic, unsigned int* result, int xsize, int ysize, int thresh);

extern void sobel_algorithm_ALL(unsigned int* pic,
                                     unsigned int* result,
                                     int xsize,
                                     int ysize,
                                     int thresh,
                                     int algorithm);

#endif